import {Component, OnInit} from '@angular/core';
import {AlertController, MenuController} from "@ionic/angular";
import {Router} from "@angular/router";
import {UserService} from "../../shared/user/user.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    public givenLogin = '';
    public givenPassword = '';

    constructor(private alertController: AlertController, private _router: Router, private menuCtrl: MenuController, private userService: UserService) {

    }

    async ionViewDidLeave() {
        await this.menuCtrl.enable(true);
    }

    async ionViewDidEnter() {
        await this.menuCtrl.enable(false);
    }

    ngOnInit() {
    }

    public async makeLogin() {
        if (!this.givenLogin || !this.givenPassword) return this.showAlert('Digite o login e a senha');

        if (this.userService.makeLogin(this.givenLogin, this.givenPassword)) await this._router.navigateByUrl('home');
        else await this.showAlert('Login e/ou senha não conferem');
    }

    private async showAlert(message: string, header: string = 'Alerta', buttonText = 'OK') {
        const alert = await this.alertController.create({
            header: header,
            subHeader: null,
            message: message,
            buttons: [buttonText]
        });

        await alert.present();
    }

}
