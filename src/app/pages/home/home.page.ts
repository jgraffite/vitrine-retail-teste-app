import {Component, ViewChild} from '@angular/core';
import {CarService} from "../../shared/car/car.service";
import {IonSlides} from "@ionic/angular";

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    @ViewChild(IonSlides, {static: false}) public ionSlide: IonSlides;

    public carTypes: {typeId: number, typeName: string, selected: boolean}[] = [];
    public carByTypes = {};

    public carListToShow = [];
    public disableSlideButton: 'prev'|'next'|null = 'prev';

    constructor(private carService: CarService) {
        this.getCars();
    }


    getCars() {
        this.carService.getCarList().subscribe(data => {
            data.forEach((item, key) => {
                this.carTypes.push({
                    typeId: item.idTipoCarro,
                    typeName: item.tipoCarro,
                    selected: (key === 0)
                });

                this.carByTypes[item.idTipoCarro] = item.carros;
                if (key === 0) this.carListToShow = item.carros;
            });

            console.log(this.carByTypes);
        });

    }

    public carTypeChanged(ev: any) {
        console.log('Segment changed', ev);
        const type = ev.detail.value;
        this.carListToShow = this.carByTypes[type];
        this.slideToStart();
    }

    public onSlideDidChange() {
        this.checkNavButtonsStatuses().then();
    }

    private slideToStart() {
        this.ionSlide.slideTo(0).then();;
        this.checkNavButtonsStatuses().then();;
    }

    public async prevSlide() {
        await this.ionSlide.slidePrev();
        this.checkNavButtonsStatuses();
    }

    public async nextSlide() {
        await this.ionSlide.slideNext();
        this.checkNavButtonsStatuses();
    }

    private async checkNavButtonsStatuses() {
        const currentIndex = await this.ionSlide.getActiveIndex();
        const totalSlides = await this.ionSlide.length();

        if (currentIndex === totalSlides-1) this.disableSlideButton = 'next';
        else if (currentIndex === 0) this.disableSlideButton = 'prev';
        else this.disableSlideButton = null;
    }

}
