import {Injectable} from '@angular/core';
import {User} from "./user";

let CURRENT_USER: User = null;

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private loginUsers = {
        "login1@email.com": {
            password: "senha1",
            name: "Usuário 1"
        },
        "login2@email.com": {
            password: "senha2",
            name: "Usuário 2"
        },
    };

    constructor() {
    }

    isAuthenticated(): boolean {
        return (CURRENT_USER !== null);
    }

    makeLogin(login, password): User|boolean {
        let tookUser = this.loginUsers[ login ];
        if (!tookUser) return false;
        if (tookUser.password != password) return false;

        CURRENT_USER = tookUser;

        return tookUser;
    }
}
