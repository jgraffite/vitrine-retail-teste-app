import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root',
})
export class CarService {

    private filePath = 'assets/cars.json';

    constructor(private http: HttpClient) {

    }

    getCarList(): any {
        return this.http.get(this.filePath)
    }
}
