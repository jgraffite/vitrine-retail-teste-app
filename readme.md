# Vitrine Retail App Teste

Consiste em exibir uma listagem de carros dividos por tipo

## Link de download do APK

Para acessar e baixar o aplicativo, [clique aqui](https://drive.google.com/file/d/1IUTnKUVf8WZflC3OiBOD23oBQa0ZXu0m/view?usp=sharing)

## Uso

Para efetuar login no sistema, utilize algum dos login abaixo:

```json
{
  "login1@email.com": "senha1",
  "login2@email.com": "senha2"
}
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
